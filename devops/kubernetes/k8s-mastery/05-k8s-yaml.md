# 05 K8s Yaml

Instead of using kubectl run, kubectl create deployment, kubectl expose, it's common to use a yaml file to configure the cluster.

```
apiVersion: v1
# PascalCase resource type
kind: Pod
metadata:
    name: nginx
# requirements
spec:
    containers:
        - name: nginx
            image: nginx: 1:17.3
---
apiVersion: v1
kind: Service
metadata:
    name: nginx
spec:
    type: NodePort
    ports:
        - port: 80
    selector:
        app: mynginx
---
apiVerson: apps/v1
king: Deployment
metadata:
    name: mynginx
spec:
    replicas: 3
    selector:
        matchLabels:
            app: mynginx
    template:
        metadata:
            labels:
                app: mynginx
        spec:
            containers:
                - name: nginx
                    image: nginx:1.17.3
```

## YAML From scratch

It's possible to pipe into a yaml file a kubctl create command in order to have a point where to start in editing the yaml file. 

```
# dry run won't start the resoure but will return the yaml file.
kubectl create deployment web --image nginx -o yaml --dry-run
kubectl create namespace awesome-app -o yaml --dry-run
```


```
# CREATE
kubectl create -f whatever.yaml
# UPSERT
kubectl apply -f whatever.yaml
```

Using a simple apply command is possible to deploy all the services, exposing their ports
```
kubectl apply -f https://k8smastery.com/dockercoins.yaml
```

It's a good practice to create a yaml file for each K8s resource:
- redis-service.yaml
- redis-deployment.yaml
- rng-service.yaml
- rng-deployment.yaml
- ...

It's possible to use kubectl apply using all files in the pwd:

```
kubectl apply -f .
```


## Daemon Sets

Resource that allows to scale a pod belong nodes, 1 pod for each node.

Ensuring that pods are equally distributed belong nodes.

Using labels and selectors is possible to identify which nodes will be used.

It's possible to create Deamon Sets only via yaml (no kubectl). 

## Labels and Selector

Selectors allow to determine which pods are part of the same pool of pods.

A selector app=rng would group up all the pods that have as selector app=rng.

```
# retrieving pods by selector
kubectl get pods -l app=rng
kubectl get pdos --selector app=rng
```

It's a good practice to assign labels to resources in order to match them with selectors; by adding a label enabled=yes it's possible to match the resource using -l enabled=yes.

It's possible to assign a label to one or more resources having an existing selector using command:

```
kubectl label pods -l app=rng enabled=true
```

It's possible to assign a label also through the yaml config file:

```
...
spec:
    ...
    selector:
        app:rng
        enabled:true
```

It's possible to use AND operator with K8s selectors:
```
POD=$(kubectl get pod -l app=rng,pod-template-hash -o name)
kubectl logs --tail 1 --follow $POD
```

Removing a label:
```
kubectl pod label -l app=rng,pod-template-hash enabled-
```

The dash at the end stands for deletion, by removing a label at runtime, all running jobs which use that selector will be stopped in real time. 