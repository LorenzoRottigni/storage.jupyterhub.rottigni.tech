# 06 K8s Dashboard

It's possible to visualize K8s resources through an official web UI.

The dashboard is usually exposed over HTTPS and requires a proper TLS cert.

Dashboard users need to authenticate, the UI will make sure that it doesn't get exposed on the internet without authentication.

## Creating a local dashboard

```
kubectl apply -f https://k8smastery.com/insecure-dashboard.yaml
kubectl get svc dashboard
```

## Unofficial Dashboards

- Kube Web View
- Kube Ops View