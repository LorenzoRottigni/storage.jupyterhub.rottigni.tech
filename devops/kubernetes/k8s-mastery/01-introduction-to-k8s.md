# 01 Introduction to kubernetes

Kubernates is a container orchestration tool similarly to Docker Swarm and AWS ECS.
It runs and manages containerized applications on a cluster (one or more servers).
It has several distros:

- Kubernetes vanilla upstream: Official K8s GitHub source code.
- Cloud managed distros: AKS, GKE, EKS, DOC.
- Self managed distros: RedHat Open Shift, Docker Enterprise, Rancher, Canonical.
- Vanilla installers: kubeadm, kops, kubicorn.
- Local development: Docker Desktop, minikube, microK8s.
- CI Testing: kind.

## Capabilities

- Start 5 containers using a specified image (e.g. atseashop/api:v1.3).
- Place an internal load balancer between specified containers.
- Place a public load balancer in front of specified containers.
- Add/remove containers at runtime and keep processing requests during updates, containers are updated one at time.
- Start a container only for the lifetime of the command from which is started or set a CRON syntax to stop the container at determined time.
- Working with docker-volumes and persisted states.
- Integrated third party services or plugins.

## Architecture

There are master nodes that controls the cluster nodes.
A cluster node includes:

- infrastructure: physical hardware where the cluster is running.
- OS: operating system running on infrastructure.
- container runtime: running docker container.
- clusters: instances of the running docker container.
- kubelet: node agent which controls the running docker engine and interacts with the master node API server.
- Kube-proxy: controls the networking between the clusters on the host.

A master node includes:

- etcd: a database containing the key-value about how configuring the environment.
- Controller Manager
- Scheduler
- API Server: It's the entrypoint for everything. It interacts with the kubelet on the cluster to manage the scheduler and the controller manager.


<img src="https://storage.rottigni.tech/fs/misc/images/k8s_infrastructure.png">

It is common to reserve a dedicated node for the control plane, a master node.

## Interacting with K8s

Kubenertes API(mostly RESTful) allows us to interact within Kubenertes clusters.
It allows to CRUD resources such as node (physical or virtual machine in our cluster), pod (group of containers running together on a node), service (stable network endpoint to connect to one or multiple containers), ...

### Pods

An abstracted concept not present in docker (and compose) that has been introduced by K8s.
A pod is the lowest deployable unit in K8s, isn't possible to touch containers directly, CRUDs are applied to pods instead.

- A pod itself is a set of one or more containers deployed on the same machine.
- Every pod has its own IP address.
- All containers inside the pod share the same network interface (localhost) and are reachable each others.
- All containers inside the pod may share volumes.
- It's important to keep in mind that Docker engine doesn't know what are pods.

It's possible to interact within a pod using shpod, it's a shell running in a pod on the cluster.
