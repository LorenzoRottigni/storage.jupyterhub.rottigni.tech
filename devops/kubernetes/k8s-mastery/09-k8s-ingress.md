# 09 K8s Ingress

Services allow to access a pod or a set or pods and is able to expose them to the internet using NodePod and LoadBalancer types.

When it's necessary to run more then one service on ports 80 and 443 on the same physical server is possible to use an Ingress resource.

The Ingress resource is designed to expose HTTP services:

- load balancing
- SSL termination
- name-based virtual hosting

The Ingress resource needs a Ingress controller as the ReplicaSet resource needs a ReplicaSet controller; for the Ingress resource the controller isn't built-in so it's necessary to use an external one.

In K8s the official Ingress controller is the Nginx Ingress controller but it's possible to use others such as Traefik, Gloo, Ambassador or Conquer.

`kubectl get ingress/ingresses/ing`

## Nginx Ingress Controller

The most common reason to use an Ingress is DNS-based routing.

It's possible to use a dummy DNS server such as nip.io which will resolve *.127.0.0.1.nip.io to 127.0.0.1. The idea is to use multiple DNS names on private network using private IP's but still going on internet to resolve that DNS address and then point back to the local machine.

- Docker Desktop hasn't a built-in Ingress installer
- Minikube has built-in Nginx installer: `minikube addons enable ingress`
  
### Yaml

- Docker Desktop: `kubectl apply -f https://k8smastery.com/ic-nginx-lb.yaml`
- Minikube: `kubectl apply -f https://k8smastery.com/ic-nginx-hn.yaml`

Checking pod status: `kubectl describe -n ingress-nginx deploy/ingress-nginx-controller`.

It's common to use yaml official templates from kubenertes/ingress-nginx project.

The two main sections in the yaml are:

- Nginx Deployment and all its required resources (Namespace, ConfigMaps (nginx configs), ServiceAccount (auth to K8s API), Role/ClusterRole/RoleBindings (auth to API parts), LimitRange (limit cpu/memory of NGINX).

- Nginx Service to expose it on 80/443

```
# DockerDesktop => create a Service with LoadBalancer, doesn't need hostNetwork arg.
kubectl apply -f https://k8smastery.com/ic-nginx-lb.yaml
# Minikube/MicroK8s, create a service using hostNetwork in order to use the local network namepsace.
kubectl apply -f https://k8smastery.com/ic-nginx-hn.yaml

# checking status
kubectl describe -n ingress-nginx deploy/nginx-ingress-controller
# all created resources on nginx namespace
kubectl get all -n ingress-nginx
```

To check Nginx healthiness is possible to visit localhost:80 when using DockerDesktop or x.x.x.x:80 K8s IP when using Minikube or MicroK8s distros.

To verify it passing through the DNS: cheddar.127.0.0.1.nip.io .

```
# service of test 1
kubectl create deployment cheddar --image=bretfisher/cheese:cheddar
# service of test 2
kubectl create deployment stilton --image=bretfisher/cheese:stilton
# service of test 3
kubectl create deployment wensleydale --image=bretfisher/cheese:wensleydale


# 3 ClusterIP services, 3 websites running on 3 different pods inside the cluster and only accessible internally.
kubectl expose deployment cheddar --port=80
kubectl expose deployment stilton --port=80
kubectl expose deployment wensleydale --port=80
```

host-based ingress resource:

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: cheddar
specs:
    rules
        - host cheddar.127.0.0.1.nip.io
            http:
                paths:
                    - path: /
                        backend:
                            serviceName: cheddar
                            servicePort:80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: stilton
specs:
    rules
        - host stilton.127.0.0.1.nip.io
            http:
                paths:
                    - path: /
                        backend:
                            serviceName: stilton
                            servicePort:80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: wensleydale
specs:
    rules
        - host wensleydale.127.0.0.1.nip.io
            http:
                paths:
                    - path: /
                        backend:
                            serviceName: wensleydale
                            servicePort:80
```

The Nginx controller talks directly to the pod instead of passing through the pod's service.

## Traefik Ingress Controller

Traefik is a proxy with built-in K8s Ingress support.

It has a web dashboard, buil-in let's encrypt, full TCP support, and more.

- Docker Desktop: `kubectl apply -f https://k8smastery.com/ic-traefik-lb.yaml`
- Minikube: `kubectl apply -f https://k8smastery.com/ic-traefik-hn.yaml`

Traefik provides a web dashboard on container port 8080.

Traefik v1.0 was using the default K8s Ingress resource, from v2.0 it uses a custom resource IngressRoute that has its own specs.