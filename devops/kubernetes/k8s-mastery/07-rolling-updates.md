# 07 Rolling Updates

Rolling updates allow to progressively deploy updates keeping the application online 24/7.

The deployment controls multiple ReplicaSets, during the rolling update there will be at least 2:

- new ReplicaSet with new target version
- old ReplicaSet with old set

It's possible to have multiple old ReplicaSet when starting another update before the first one is done.

There are 2 important arguments:

- maxUnavailable: the number of pods that will always be available.
- maxSurge: the limit of replicas+maxSurge pods in total.

retrieveing maxUnavailable and maxSurge configs for a deployment: 
```
kubectl get deploy -o json | jq ".items[] | {name:.metadata.name} + .spec.strategy.rollingUpdate"
```

It's possible to do rolling updates with: Deployments, DaemonSets, StatefulSets. 

Editing on of these resources will automatically result in a rolling update.

It's possible to monitor rolling updates using command: `kubectl rollout`.

Starting a rolling update by editing a deploy image:

```
kubectl set image deploy worker worker=dockercoins/worker:v0.2
```

When K8s sends a shut down signal to the pods, Docker will send a shut down signal to the container and linux will send a shut down signal to the application process.

When dealing with NodeJS or Python they don't accept linux kill signals so it's necessary to shut down these applications properly.

Retrieiving information about the rolling update strategy:
```
kubectl describe deploy worker
```

Cancelling a deployment:
```
kubectl rollout undo deploy worker
kubectl rollout status deploy worker
```

kubectl rollout undo can't be used more then once.

It is possible to get the rollout history using command `kubectl rollout history deployment worker`.

To switch back to the desired version it's possible to use the --to-revision flag: `kubectl rollout undo deployment worker --to-revision=1`

