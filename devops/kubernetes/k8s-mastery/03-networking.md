# 03 Networking

## Services

Pods are reachable each others through IP addresses, a stable DNS is preferrable.

K8s exposes a Service resource type which is a stable address for a pod (or a bunch of pods).

`kubectl expose` creates a service for existing pods.

To safely connect to a pod(s) it's necessary to create a service.

Once a service is created, CoreDNS will resolve it by name (service hello will resolve for "hello").

There are several types of services:

- ClusterIP: default one, a virtual IP address is allocated for the service which is reachable only from within the cluster (nodes and pods). It's possible to connect to the service using original port number.
- NodePort: A random port is allocated for the service and it's available to everybody. It's necessary to keep the random port updated from the client.
- LoadBalancer: An external load balancer is allocated for the service. A NodePort will be created and the load balancer will sent traffic to that port. Available only when the unerlying infrastructure provides some "load balancer as a servicer" (AWS, Azure, GCE, OpenStack, ...)
- ExternalName: The DNS entry managed by CoreDNS will just be a CNAME to a provided record (no port, no IP address, nothing else allocated).

```
# new deployment with bretfisher/httpenv image, a web server running on 8888
kubectl create deployment httpenv --image=bretfisher/httpenv
# scale httpenv deploy to 10 instances
kubectl scale deployment httpenv --replicas 10
# create the service type ClusterIP for port 8888
kubectl expose deployment httpenv --port 8888
kubectl get services
```

### Debugging

```
# creates shpod pod into shpod namespace
kubectl apply -f https://bret.run/shpod.yml
# attach shell for shpod pod in shpod namespace
kubectl attach --namespace=shpod -it shpod

# retrieving the service IP address
IP=$(kubectl get service httpenv -o go-template --template '{{ .spec.clusterIP }}')

curl http://$IP:8888

# revert apply
kubectl delete -f https://bret.run/shpod.yml
```

### Headeless Services

A headless service is obtained by setting the clusterIP to None, so the service won't have a virtual IP address.
This is useful when you don't want to use a load balancer, since there's no a virtual IP.

### Service Endpoints

A service is composed by a number of endpoints.
Each endpoint is represented by host+port where the service is available.
Endpoints are maintained and updated automatically by K8s.

```
# retrieving service endpoints
kubectl describe service httpenv (-o yaml)
# option 2
kubectl get pods -l app=httpenv -o wide
```

! Resource endpoints is always plural.

### Exposing services to the world

The default ClusterIP only works for internal traffic.
When is necessary to accept external traffic are used:

- NodePort (expose a service on a TCP port)
- LoadBalancer (cloud load balancer for the service)
- ExternalIP (use one node's external IP address)
- Ingress (a special mechanism for HTTP services)

## Kubernetes Network Model.

The cluster (nodes ands pods) is one big flag IP network.

- All nodes must be able to reach each other, without NAT
- All pods must be able to reach each other, without NAT
- Pods and nodes must be able to reach each other, without NAT
- Each pod is aware of its IP address, without NAT
- Pod IP addresses are assigned by the network implementation

Everything in the K8s network can reach everything, there's no address translation and the network implementation can decide how to allocate addresses.

Most K8s clusters use CNI (**Container Network Interface**) plugins to implement networking; when a pod is created, K8s delegates the network setup to these plugin(s)..
A CNI plugin will:

- allocate an IP address (by calling an IPAM (IP Address Management) plugin)
- add a network interface into the pod's network namespace
- configure the interface as well as required routes

Minikube for example uses Kubenet, a great CNI plugin for single node clusters.

- pod-to-pod network => provides communication between pods and nodes, implemented with CNI plugins.
- pod-to-service network => internal communication and load balancing, implemented with kube-proxy or kube-router.
- Network policies => firewalling and isolation.

```
Kubernetes Cluster
|
|-- Master Server (Control Plane)
|   |-- API Server
|   |-- Scheduler
|   |-- Controller Manager
|   |-- etcd (Cluster Store)
|
|-- Node
|   |-- Kubelet
|   |-- Container Runtime
|   |-- Pods (running containers)
|
|-- Node
|   |-- Kubelet
|   |-- Container Runtime
|   |-- Pods (running containers)
|
|-- Node
|   |-- Kubelet
|   |-- Container Runtime
|   |-- Pods (running containers)
...
```