# 08 Healthchecks

Healthchecks are key to providing built-in lifecycle automation.

Healthchecks are probes that apply to containers (not to pods).

Kubernetes will take action on containers that fail healthchecks.

Each container can have 3 optional probes:

## Liveness Probe 

- Is container dead or alive?
- A dead container can't come back to life.
- If liveness probe fails, the container is killed.
-  there's a restartPolicy: Never | OnFailure | Always

## Readiness Probe

- Is container ready to serve traffic?
- Indicate failure due to an external cause (db down, auth or other service unvailable)
- Indicate failure for unvailability (max N parallel connections, runtime busy)

## Startup Probe

- Is container bootstrapping?
- Container not ready yet.
- Fixes the needing of a initialDelaySeconds parameter

There are 3 probe handlers:

- HTTP: HTTP status code between 200 and 399
- TCP: TCP port open
- program execution: command executed inside the container (process exit 0 is success).

```
apiVersion: v1
King: Pod
metadata:
    name: rng
spec:
    containers:
        - name: rng
          image: dockercoins/rng:v0.1
          livenessProbe:
            httpGet:
                path: /
                port: 80
            initialDelaySeconds: 10
            periodSeconds: 1
```

## Generating Traffic

Using Apache Bench to send concurrent requests to the rng in order to stress it:

```
# 1000 requests x 10 concurrency
kubectl attach --namespace=shpod -ti shpod ab -c 10 -n 1000 http://<ClusterIP>/1
```

Monitoring events + pods:

```
kubectl get events -w
kubectl get pods -w
```

## Healthchecks for Redis

Redis isn't capable to handle TCP/HTTP requests so it's necessary an exec probe running `redis-cli ping`.

To handle zombie-processes it's common to use a tool named "tini" to run the ping command.

It's possible to add tini directly inside the Dockerfile to include it in the container:

```
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/$(TINY_VERSION)/tini /tini
RUN chmod +x /tini
```

```
containers:
    - name: redis
        image: custom-redis-image
        livenessProbe:
            exec:
                command:
                    - /tini
                    - -s
                    - --
                    - redis-cli
                    - ping
            initalDelaySeconds: 30
            periodSeconds: 5
```

example redis repo: `https://github.com/BretFisher/redis-tini`.
